/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2023 ycz. All rights reserved.
///
/// Created by ycz on 2023/1/3.
///
/// @file  y_linklist.h
///
/// @brief
///     y_linklist 是一种用于嵌入式设备的链表，可以方便用于链式数据缓存与修改
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_LINKLIST_H
#define _Y_LINKLIST_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_LINKLIST_MAJOR 0  ///< 版本信息 主版本   ( 主架构变化 )
#define Y_LINKLIST_MINOR 1  ///< 版本信息 次版本   ( 单个功能增加或修改 )
#define Y_LINKLIST_PATCH 0  ///< 版本信息 补丁版本  ( bug修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

typedef struct linklist LINKLIST_st;  ///< linklist 实例结构体 对外隐藏具体实现 当不透明指针使用



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 枚举
/// ------------------------------------------------------------------------------------------------------------------------------------

typedef enum {
    SINGLE_LINKLIST,  ///< 单向链表
    DOUBLE_LINKLIST,  ///< 双向链表
} LINKLIST_TYPE_e;    ///< 链表类型



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void         y_linklist_print_version();                                                                     ///< 打印 linklist 版本号
void         y_linklist_print_data_addr(LINKLIST_st *linklist);                                              ///< 打印所有节点数据的地址
uint16_t     y_linklist_get_num(LINKLIST_st *linklist);                                                      ///< 获取节点总数量
int16_t      y_linklist_find_index(LINKLIST_st *linklist, void *node_data, uint16_t data_size);              ///< 查找一个节点数据的位置
void        *y_linklist_find(LINKLIST_st *linklist, uint16_t index);                                         ///< 查找一个节点的数据 下标从 0 开始 失败 : -1
bool         y_linklist_insert(LINKLIST_st *linklist, uint16_t index, void *node_data, uint16_t data_size);  ///< 插入一个节点
bool         y_linklist_add(LINKLIST_st *linklist, void *node_data, uint16_t data_size);                     ///< 添加一个节点
bool         y_linklist_delete(LINKLIST_st *linklist, uint16_t index);                                       ///< 删除一个节点
bool         y_linklist_delete_all(LINKLIST_st *linklist);                                                   ///< 删除所有节点
bool         y_linklist_is_full(LINKLIST_st *linklist);                                                      ///< 判断链表是否为满
bool         y_linklist_is_empty(LINKLIST_st *linklist);                                                     ///< 判断链表是否为空
bool         y_linklist_destroy(LINKLIST_st *linklist);                                                      ///< 销毁一个链表
LINKLIST_st *y_linklist_create(LINKLIST_TYPE_e linklist_type, bool is_cycle, uint16_t length);               ///< 创建一个链表



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_LINKLIST_H 防止当前头文件被重复引用