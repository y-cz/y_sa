/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2023 ycz. All rights reserved.
///
/// Created by ycz on 2023/4/23.
///
/// @file  y_treelist.h
///
/// @brief
///     y_treelist 是一种用于嵌入式设备的多叉表，可以方便用于树形数据缓存与修改
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_TREELIST_H
#define _Y_TREELIST_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_TREELIST_MAJOR 0  ///< 版本信息 主版本   ( 主架构变化 )
#define Y_TREELIST_MINOR 1  ///< 版本信息 次版本   ( 单个功能增加或修改 )
#define Y_TREELIST_PATCH 0  ///< 版本信息 补丁版本  ( bug修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体及函数原型
/// ------------------------------------------------------------------------------------------------------------------------------------

typedef struct treelist      TREELIST_st;       ///< treelist 实例结构体 对外隐藏具体实现 当不透明指针使用
typedef struct treelist_node TREELIST_NODE_st;  ///< treelist_node 实例结构体 对外隐藏具体实现 当不透明指针使用
typedef void (*delete_cb_fun)(void *data);      ///< 删除回调



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void              y_treelist_print_version();                                                                       ///< 打印版本号
void              y_treelist_print_node_table(TREELIST_st *treelist);                                               ///< 打印节点表
uint16_t          y_treelist_get_layer(TREELIST_st *treelist);                                                      ///< 获取节点总层级
uint16_t          y_treelist_get_num(TREELIST_st *treelist);                                                        ///< 获取节点总数
uint16_t          y_treelist_get_node_layer(TREELIST_NODE_st *node);                                                ///< 获取节点当前层级
uint16_t          y_treelist_get_child_num(TREELIST_NODE_st *node);                                                 ///< 获取孩子总数
bool              y_treelist_is_child(TREELIST_NODE_st *father, TREELIST_NODE_st *child);                           ///< 判断是否为父子关系
void             *y_treelist_get_data(TREELIST_NODE_st *node);                                                      ///< 获取节点数据
TREELIST_NODE_st *y_treelist_traverse_next(TREELIST_st *treelist, TREELIST_NODE_st *root, TREELIST_NODE_st *node);  ///< 获取下一个遍历节点
TREELIST_NODE_st *y_treelist_find_ancestor(TREELIST_NODE_st *node, uint16_t ancestor_layer);                        ///< 查找节点第 n 层的祖先
void             *y_treelist_find_index_data(TREELIST_st *treelist, uint16_t index);                                ///< 查找索引对应的节点的数据
TREELIST_NODE_st *y_treelist_find_index(TREELIST_st *treelist, uint16_t index);                                     ///< 查找索引对应的节点
TREELIST_NODE_st *y_treelist_find(TREELIST_st *treelist, TREELIST_NODE_st *node, void *data, uint16_t size);        ///< 查找数据对应的节点
bool              y_treelist_add(TREELIST_st *treelist, TREELIST_NODE_st *father_node, void *data, uint16_t size);  ///< 添加节点
bool              y_treelist_delete(TREELIST_st *treelist, TREELIST_NODE_st *node, delete_cb_fun delete_cb);        ///< 删除节点
bool              y_treelist_clear(TREELIST_st *treelist, delete_cb_fun delete_cb);                                 ///< 清空节点
bool              y_treelist_destroy(TREELIST_st *treelist, delete_cb_fun delete_cb);                               ///< 销毁多叉表
TREELIST_st      *y_treelist_create();                                                                              ///< 创建多叉表



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_TREELIST_H 防止当前头文件被重复引用