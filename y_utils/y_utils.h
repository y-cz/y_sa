/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2023 ycz. All rights reserved.
///
/// Created by ycz on 2023/7/30.
///
/// @file  y_utils.h
///
/// @brief
///     y_utils 是一些常用工具函数的集合
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_UTILS_H
#define _Y_UTILS_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_UTILS_MAJOR    0  ///< 主版本     ( 主架构变化 )
#define Y_UTILS_MINOR    1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_UTILS_PATCH    0  ///< 补丁版本   ( bug 修复 )

#define MAC_FORMAT       "%02X%02X%02X%02X%02X%02X"                                      ///< mac 打印格式
#define MAC_TO_STR(mac)  mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]                  ///< mac 打印格式
#define MAC8_FORMAT      "%02X%02X%02X%02X%02X%02X%02X%02X"                              ///< mac 打印格式
#define MAC8_TO_STR(mac) mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], mac[6], mac[7]  ///< mac 打印格式
#define FLOAT_FORMAT     "%c%ld.%02ld"                                                   ///< float 打印格式
#define FLOAT_PRINT(X)                                                                                                                                                             \
    (X) < 0 ? '-' : ' ', (uint32_t) ((X) < 0 ? -(X) : (X)), (uint32_t) ((((X) - (int) (X)) * 100) < 0 ? -(((X) - (int) (X)) * 100) : (((X) - (int) (X)) * 100))  ///< float 打印格式



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 版本号结构体
typedef struct {
    uint8_t major;  ///< 主版本
    uint8_t minor;  ///< 次版本
    uint8_t patch;  ///< 补丁版本
} VERSION_st;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 全局变量
/// ------------------------------------------------------------------------------------------------------------------------------------

extern uint8_t g_mac6_empty[6];      ///< {0X00, 0X00, 0X00, 0X00, 0X00, 0X00};              全局 6位 空 mac 地址
extern uint8_t g_mac6_root[6];       ///< {0XEF, 0XEF, 0XEF, 0XEF, 0XEF, 0XEF};              全局 6位  root 地址
extern uint8_t g_mac6_broadcast[6];  ///< {0XEE, 0XEE, 0XEE, 0XEE, 0XEE, 0XEE};              全局 6位 广播地址
extern uint8_t g_mac8_empty[8];      ///< {0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00, 0X00};  全局 8位 空 mac 地址
extern uint8_t g_mac8_root[8];       ///< {0XEF, 0XEF, 0XEF, 0XEF, 0XEF, 0XEF, 0XEF, 0XEF};  全局 8位  root 地址
extern uint8_t g_mac8_broadcast[8];  ///< {0XEE, 0XEE, 0XEE, 0XEE, 0XEE, 0XEE, 0XEE, 0XEE};  全局 8位 广播地址



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void       y_utils_print_version();                                                             ///< 打印模块版本号
uint8_t    y_utils_get_sum8(uint8_t *data, uint16_t size, bool is_invert);                      ///< 获取 sum8 检验值
uint16_t   y_utils_get_sum16(uint8_t *data, uint16_t size, bool is_invert);                     ///< 获取 sum16 检验值
uint16_t   y_utils_get_crc16(uint8_t *data, uint32_t size);                                     ///< 获取 crc16 检验值
uint8_t   *y_utils_big_little_swap(uint8_t *p, uint8_t size);                                   ///< 大小端转换
bool       y_utils_cmp_mac6(uint8_t *mac1, uint8_t *mac2);                                      ///< 比较 6位 mac 地址是否相同
bool       y_utils_cmp_mac8(uint8_t *mac1, uint8_t *mac2);                                      ///< 比较 8位 mac 地址是否相同
uint8_t   *y_utils_str_to_mac6(uint8_t *mac_str);                                               ///< 6位 mac 字符串 转字节
uint8_t   *y_utils_str_to_mac8(uint8_t *mac_str);                                               ///< 8位 mac 字符串 转字节
uint8_t   *y_utils_mac6_to_str(uint8_t *mac);                                                   ///< 字节 转 6位 mac 字符串
uint8_t   *y_utils_mac8_to_str(uint8_t *mac);                                                   ///< 字节 转 8位 mac 字符串
VERSION_st y_utils_str_to_version(uint8_t *version_str);                                        ///< 字符串转版本号
uint8_t   *y_utils_version_to_str(VERSION_st version);                                          ///< 版本号转字符串
int8_t     y_utils_cmp_version(VERSION_st ver1, VERSION_st ver2);                               ///< 比较版本号死否相同 0=相同  -1=ver1<ver2  1=ver1>ver2
int8_t     y_utils_cmp_version2(VERSION_st ver1, uint8_t major, uint8_t minor, uint8_t patch);  ///< 比较版本号死否相同 0=相同  -1=ver1<ver2  1=ver1>ver2
void       y_utils_crc32_restart();                                                             ///< crc32 重新统计
uint32_t   y_utils_crc32_calc(uint8_t *date, uint32_t size);                                    ///< crc32 计算



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_UTILS_H 防止当前头文件被重复引用