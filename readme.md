<p align="left">
	<a target="_blank" href="https://gitee.com/y-cz/y_log/blob/master/LICENSE">
		<img src="https://img.shields.io/badge/license-MIT-green" ></img>
	</a>
</p>

## Y_SA

### 1. Y_SA 介绍

Y_SA 全称 y simple architecture，一套用于嵌入式开发的简单架构，可以方便的复用代码进行功能开发，快速构架示例，实现高效工作，按时下班的生活平衡。

### 2. Y_SA 使用说明

Y_SA 目前由一系列代码库组成。

示例:

```c
// 暂无示例，敬请期待！
```

### 3. 版本说明

#### V0.1.0

1. 初始版本, 整合代码库