/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2022 ycz. All rights reserved.
///
/// Created by ycz on 2022/1/12.
///
/// @file  y_ringbuf.h
///
/// @brief
///     y_ringbuf 是一种用于嵌入式设备的 ringbuf，可以方便用于数据缓存与传递。
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_RINGBUF_H
#define _Y_RINGBUF_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdbool.h>
#include <stdint.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_RINGBUF_VERSION_MAJOR  0  ///< 版本信息 主版本   ( 主架构变化 )
#define Y_RINGBUF_VERSION_MINOR  1  ///< 版本信息 次版本   ( 单个功能增加或修改 )
#define Y_RINGBUF_VERSION_PATCH  0  ///< 版本信息 补丁版本  ( bug修复 )

#define Y_RINGBUF_USE_MUTEX_LOCK 0  ///< 是否使用互斥锁  在使用单线程的情况下可以关闭以降低开销 \n 0 : 不使用  1 : 使用



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

typedef struct ringbuf RINGBUF_st;  ///< ring_buf 实例结构体 对外隐藏具体实现 当不透明指针使用



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void        y_ringbuf_print_version();                                                   ///< 打印模块版本号
uint16_t    y_ringbuf_get_used_size(RINGBUF_st *ringbuf);                                ///< 获取已用字节数
uint16_t    y_ringbuf_get_free_size(RINGBUF_st *ringbuf);                                ///< 获取可用字节数
bool        y_ringbuf_find_chr(RINGBUF_st *ringbuf, uint8_t ch, uint16_t *index);        ///< 查找字符第一次出现的位置
uint16_t    y_ringbuf_read_clear(RINGBUF_st *ringbuf, uint8_t *data, uint16_t size);     ///< 读取数据并删除
uint16_t    y_ringbuf_read_only(RINGBUF_st *ringbuf, uint8_t *data, uint16_t size);      ///< 读取数据但不删除
bool        y_ringbuf_write(RINGBUF_st *ringbuf, uint8_t *data, uint16_t size);          ///< 写入数据
bool        y_ringbuf_delete_index(RINGBUF_st *ringbuf, uint16_t index, uint16_t size);  ///< 删除指定长度数据 从指定位置开始删除
bool        y_ringbuf_delete_data(RINGBUF_st *ringbuf, uint16_t size);                   ///< 删除指定长度数据 从头开始删除
bool        y_ringbuf_is_full(RINGBUF_st *ringbuf);                                      ///< 判满
bool        y_ringbuf_is_empty(RINGBUF_st *ringbuf);                                     ///< 判空
bool        y_ringbuf_reset(RINGBUF_st *ringbuf);                                        ///< 重置
bool        y_ringbuf_destroy(RINGBUF_st *ringbuf);                                      ///< 销毁
RINGBUF_st *y_ringbuf_create(uint16_t length);                                           ///< 创建 ringbuf



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_RINGBUF_H 防止当前头文件被重复引用
