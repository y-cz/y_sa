/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2024 ycz. All rights reserved.
///
/// Created by ycz on 2024/3/20.
///
/// @file  test_case.c
///
/// @brief
///     y_sa 单元测试
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>

#include "y_sa.h"



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 私有函数
/// ------------------------------------------------------------------------------------------------------------------------------------

/// @brief   y_log 单元测试
/// @retval  true                          成功
/// @retval  false                         失败
static bool y_log_test() {

    // 开始测试
    YLOG_CRLF();
    YLOGI("------------------------------------------  y_log_test  start  ------------------------------------------");

    // 要带时间打印 请将 Y_LOG_SWITCH_TIME  设置为 1  并设置获取时间字符串函数 Y_TIME_STR
    // 要带颜色打印 请将 Y_LOG_SWITCH_COLOR 设置为 1

    // 1. 断言测试
    // 表达式为真  断言成功 程序继续执行
    // 表达式为假  断言失败 程序立即返回
    // YLOGA(1);        // 断言失败 则返回但无返回值
    YLOGA_FALSE(1);  // 断言失败 则返回 false
    // YLOGA_NULL(1);   // 断言失败 则返回 NULL

    // 2. 打印 各种格式日志 (结束后默认换行)
    YLOGE("HELLO,YLOG!!!");
    YLOGW("HELLO,YLOG!!!");
    YLOGI("HELLO,YLOG!!!");
    YLOGD("HELLO,YLOG!!!");
    YLOGV("HELLO,YLOG!!!");

    // 3.打印 16 进制格式数据 (结束后默认换行)
    unsigned char buf[8] = {1, 2, 3, 4, 5, 6, 7, 8};
    YLOG_DATA("DATA", buf, 8);

    // 4.打印模块版本号 (结束后默认换行)
    YLOG_VERSION("test_module", 1, 2, 3);

    // 5.打印初始化结果 (结束后默认换行)
    // 表达式为真  初始化成功
    // 表达式为假  初始化失败
    YLOG_INIT(true);

    // 6.打印换行
    YLOGI("LF");
    YLOG_LF();
    YLOGI("CR");
    YLOG_CR();
    YLOGI("CRLF");
    YLOG_CRLF();

    // 结束测试
    YLOGI("------------------------------------------  y_log_test  OK  ------------------------------------------");

    return true;
}

/// @brief   y_alg 单元测试
/// @retval  true                          成功
/// @retval  false                         失败
static bool y_alg_test() {

    // 截断 test
    uint8_t num = 3;
    double  a   = 1.222222222222233333333332;
    double  b   = y_alg_trunc(a, num);
    YLOGI("%.15f  trunc %d  = %.15f", a, num, b);

    // 窗口 test
    ALG_WINDOW_st *win = y_alg_window_create(10, 2, true);

    // for (int i = 1; i < 100; ++i) {
    //     y_alg_window_add(win, i);
    //     // y_alg_window_clear(win);
    //     y_alg_window_print(win);
    //     // YLOGI("is full %d", y_alg_window_is_full(win));
    // }



    YLOGI("%f", fmodf(2.2, 2));

    return true;
}

/// @brief   底层发送数据
/// @param   [in] data                     数据
/// @param   [in] size                     大小
/// @return  实际发送数据大小
uint16_t _jt_modbus_modbus_write(uint8_t *data, uint16_t size) {
    YLOG_DATA("MODBUS SEND", data, size);
    return true;
}


/// @brief   y_modbus 单元测试
/// @retval  true                          成功
/// @retval  false                         失败
static bool y_modbus_test() {

    MODBUS_st *master = y_modbus_init(MODBUS_MASTER);

    master->write     = _jt_modbus_modbus_write;
    y_modbus_master_read_data_async(master, 11, 0, 1);
    y_modbus_master_write_reg_async(master, 11, 1, 12);
    uint16_t data[2] = {2, 3};
    y_modbus_master_write_data_async(master, 11, 1, 2, (uint8_t *) data);

    return true;
}

/// ------------------------------------------------------------------------------------------------------------------------------------
/// 主函数
/// ------------------------------------------------------------------------------------------------------------------------------------

int y_sa_test() {

    // 打印各模块版本号
    y_sa_print_version();

    // 单元测试
    // y_alg_test();
    // y_log_test();
    // y_modbus_test();

    uint8_t            send_mac[6] = {0x12, 0x12, 0x12, 0x12, 0x12, 0x12};
    uint8_t            recv_mac[6] = {0x22, 0x22, 0x22, 0x22, 0x22, 0x22};
    PROTOCOL_M2M_V1_st msg         = y_protocol_create_m2m_msg(2, send_mac, recv_mac, 1, 1, 3, 7, 5, NULL, 0);
    y_protocol_print_m2m_msg(&msg);
    PROTOCOL_M2M_V1_st msg1         = y_protocol_create_m2m_msg(2, send_mac, recv_mac, 2, 1, 3, 7, 5, NULL, 0);
    y_protocol_print_m2m_msg(&msg1);


    return 0;
}
