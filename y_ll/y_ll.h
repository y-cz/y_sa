/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2024 ycz. All rights reserved.
///
/// Created by ycz on 2024/3/20.
///
/// @file  y_ll.h
///
/// @brief
///     y_ll 是对嵌入式芯片平台底层接口的封装 low interface
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_LL_H
#define _Y_LL_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include "y_time.h"
#include "y_utils.h"



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

/// SYS 设置  软件系统版本  这里改
#define Y_LL_SW_MAJOR      0  ///< 主版本     ( 主架构变化 )
#define Y_LL_SW_MINOR      0  ///< 次版本     ( 单个功能增加或修改 )
#define Y_LL_SW_PATCH      1  ///< 补丁版本   ( bug 修复 )

/// sys 信息设置
#define Y_LL_DEV_TYPE      0X1  ///< 设备默认类型
#define Y_LL_DEV_NUM       11   ///< 设备默认编号 （也是 modbus 默认地址）

/// NVS 设置
#define Y_LL_NVS_ADDR      0x08060000  ///< NVS 表开始地址
#define Y_LL_NVS_SIZE      8192        ///< NVS 大小 只有 2048 4096 8192 16384 四种选择

/// OTA 分区设置
#define Y_LL_OTA_ADDR_MIAN 0x01012000  ///< 主分区固件存放地址
#define Y_LL_OTA_ADDR_BAK  0x01052000  ///< 备份分区固件存放地址

/// 本模块版本
#define Y_LL_MAJOR         0  ///< 主版本     ( 主架构变化 )
#define Y_LL_MINOR         1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_LL_PATCH         0  ///< 补丁版本   ( bug 修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void       y_ll_print_version();                                          ///< 打印模块版本号
void       y_ll_print_mem_info();                                         ///< 打印内存使用信息
size_t     y_ll_get_use_mem();                                            ///< 获取已使用的内存大小
void      *y_ll_malloc(size_t size);                                      ///< 动态分配内存
void       y_ll_free(void *p);                                            ///< 释放内存
void       y_ll_jump_addr(uint32_t addr);                                 ///< 跳到指定地址运行
void       y_ll_reboot();                                                 ///< 系统重启
void       y_ll_delay(uint32_t ms);                                       ///< 系统延时 ms
VERSION_st y_ll_get_hardware_version();                                   ///< 获取硬件版本号
uint8_t   *y_ll_get_mac();                                                ///< 获取获取 mac 地址
bool       y_ll_flash_erase(uint32_t addr, uint32_t size);                ///< 擦除
uint32_t   y_ll_flash_read(uint32_t addr, uint8_t *buf, uint32_t size);   ///< 读取
uint32_t   y_ll_flash_write(uint32_t addr, uint8_t *buf, uint32_t size);  ///< 写入
TIME_st    y_ll_time_get();                                               ///< 获取时间
bool       y_ll_time_set(TIME_st *time);                                  ///< 设置时间



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_LL_H 防止当前头文件被重复引用