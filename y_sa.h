/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2024 ycz. All rights reserved.
///
/// Created by ycz on  2024/4/20.
///
/// @file  y_sa.h
///
/// @brief
///     Y_SA 全称 y simple architecture，一套用于嵌入式开发的简单工具集合，可以方便的复用代码进行功能开发，快速构架示例，实现高效工作，按时下班的生活平衡。
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_SA_H
#define _Y_SA_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include "y_alg.h"
#include "y_cli.h"
#include "y_comm_m2m.h"
#include "y_linklist.h"
#include "y_ll.h"
#include "y_log.h"
#include "y_lora.h"
#include "y_modbus.h"
#include "y_nvs.h"
#include "y_ota.h"
#include "y_protocol.h"
#include "y_ringbuf.h"
#include "y_sys.h"
#include "y_time.h"
#include "y_timer.h"
#include "y_treelist.h"
#include "y_utils.h"



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_SA_MAJOR 0  ///< 主版本     ( 主架构变化 )
#define Y_SA_MINOR 1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_SA_PATCH 0  ///< 补丁版本   ( bug 修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 打印 y_sa 版本信息
static inline void y_sa_print_version() {
    // Y_SA 版本
    YLOG_VERSION("Y_SA", Y_SA_MAJOR, Y_SA_MINOR, Y_SA_PATCH);
    // 各模块版本
#ifdef _Y_ALG_H
    y_alg_print_version();
#endif
#ifdef _Y_CLI_H
    y_cli_print_version();
#endif
#ifdef _Y_Y_COMM_M2M_H
    y_comm_m2m_print_version();
#endif
#ifdef _Y_LINKLIST_H
    y_linklist_print_version();
#endif
#ifdef _Y_LL_H
    y_ll_print_version();
#endif
#ifdef _Y_LOG_H
    y_log_print_version();
#endif
#ifdef _Y_LORA_H
    y_lora_print_version();
#endif
#ifdef _Y_MODBUS_H
    y_modbus_print_version();
#endif
#ifdef _Y_NVS_H
    y_nvs_print_version();
#endif
#ifdef _Y_OTA_H
    y_ota_print_version();
#endif
#ifdef _Y_PROTOCOL_H
    y_protocol_print_version();
#endif
#ifdef _Y_RINGBUF_H
    y_ringbuf_print_version();
#endif
#ifdef _Y_SYS_H
    y_sys_print_version();
#endif
#ifdef _Y_TIME_H
    y_time_print_version();
#endif
#ifdef _Y_TIMER_H
    y_timer_print_version();
#endif
#ifdef _Y_TREELIST_H
    y_treelist_print_version();
#endif
#ifdef _Y_UTILS_H
    y_utils_print_version();
#endif
}



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_SA_H 防止当前头文件被重复引用