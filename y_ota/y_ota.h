/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2024 ycz. All rights reserved.
///
/// Created by ycz on 2024/1/2.
///
/// @file  y_ota.h
///
/// @brief
///     y_ota 是一种用于嵌入式的简单 ota 库
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_OTA_H
#define _Y_OTA_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include "y_utils.h"



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_OTA_MAJOR 0  ///< 主版本     ( 主架构变化 )
#define Y_OTA_MINOR 1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_OTA_PATCH 0  ///< 补丁版本   ( bug 修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

// OTA 信息结构体
typedef struct {
    VERSION_st ver;    ///< 版本号
    uint32_t   addr;   ///< 分区开始地址
    uint32_t   size;   ///< 分区大小
    uint32_t   crc32;  ///< crc32 检验值
} OTA_INFO_st;

// OTA 参数结构体
typedef struct {
    OTA_INFO_st app_main;  ///< 主分区
    OTA_INFO_st app_bak;   ///< 备份分区
} OTA_CONFIG_st;

#pragma pack(1)  // 设置结构体 1 字节对齐

// OTA 升级包结构体
typedef struct {
    VERSION_st ver;         ///< 版本号
    uint32_t   crc32;       ///< crc32 检验值
    uint32_t   total_size;  ///< 固件总大小
    uint32_t   offset;      ///< 当前包偏移地址
    uint16_t   size;        ///< 当前包数据大小
    uint8_t    sum;         ///< 当前包检验和
    uint8_t   *data;        ///< 当前包数据
} OTA_PACK_st;

#pragma pack()  // 恢复结构体默认对其方式



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void           y_ota_print_version();                      ///< 打印模块版本号
void           y_ota_auto_switch();                        ///< 分区自动选择
void           y_ota_config_print(OTA_CONFIG_st *config);  ///< 打印参数
uint8_t        y_ota_upgrade(OTA_PACK_st pack);            ///< 写入升级包
OTA_CONFIG_st *y_ota_config_get();                         ///< 参数获取
bool           y_ota_config_set(OTA_CONFIG_st *config);    ///< 参数设置
void           y_ota_boot_init();                          ///< boot 初始化
void           y_ota_init();                               ///< 初始化



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_OTA_H 防止当前头文件被重复引用