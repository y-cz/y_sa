/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2023 ycz. All rights reserved.
///
/// Created by ycz on 2023/12/5.
///
/// @file  y_modbus.h
///
/// @brief
///     y_modbus 一种用于嵌入式设备的简单 modbus 通信
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_MODBUS_H
#define _Y_MODBUS_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include "y_ringbuf.h"



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_MODBUS_MAJOR 0  ///< 主版本     ( 主架构变化 )
#define Y_MODBUS_MINOR 1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_MODBUS_PATCH 1  ///< 补丁版本   ( bug 修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 枚举
/// ------------------------------------------------------------------------------------------------------------------------------------

/// MODBUS 类型
typedef enum {
    MODBUS_MASTER,  ///< 主机
    MODBUS_SLAVE,   ///< 从机
} MODBUS_TYPE_e;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

/// MODBUS 句柄
typedef struct {
    MODBUS_TYPE_e type;                                                             ///< MODBUS 类型
    RINGBUF_st   *ringbuf;                                                          ///< ringbuf
    void (*business)(uint16_t addr, uint8_t fun_id, uint8_t *data, uint16_t size);  ///< 主机业务回调处理-异步模式
    uint16_t (*write)(uint8_t *data, uint16_t size);                                ///< 发送数据底层回调
    uint16_t (*reg_get)(uint16_t reg);                                              ///< 从机获取寄存器
    bool (*reg_set)(uint8_t addr, uint16_t reg, uint16_t data);                     ///< 从机设置寄存器
} MODBUS_st;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

/// note: 目前仅支持  以下功能码
///     3  (读取多个寄存器的值)
///     4  (读取多个寄存器的值)
///     6  (写入单个寄存器的值)
///     16 (写入多个寄存器的值)

void       y_modbus_print_version();                                                                                      ///< 打印模块版本号
bool       y_modbus_master_read_data_async(MODBUS_st *handle, uint8_t addr, uint16_t reg, uint16_t num);                  ///< 主机 异步读取多个寄存器
bool       y_modbus_master_write_reg_async(MODBUS_st *handle, uint8_t addr, uint16_t reg, uint16_t data);                 ///< 主机 异步写单个寄存器
bool       y_modbus_master_write_data_async(MODBUS_st *handle, uint8_t addr, uint16_t reg, uint16_t num, uint8_t *data);  ///< 主机 异步写多个寄存器
bool       y_modbus_add_data(MODBUS_st *handle, uint8_t *data, uint16_t size);                                            ///< 添加待处理数据
MODBUS_st *y_modbus_init(MODBUS_TYPE_e type);                                                                             ///< 初始化
void       y_modbus_process(MODBUS_st *handle, uint32_t time_ms);                                                         ///< 任务处理



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_MODBUS_H 防止当前头文件被重复引用