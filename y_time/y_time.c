/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2023 ycz. All rights reserved.
///
/// Created by ycz on 2023/10/26.
///
/// @file  y_time.c
///
/// @brief
///     y_time 是一种时间相关接口的系统抽象
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include "y_time.h"

#include "y_sa.h"
#include <time.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 全局变量
/// ------------------------------------------------------------------------------------------------------------------------------------

static uint64_t g_default_timestamp = 946656000000;  // 默认时间戳 2000-1-1 00:00:00
static uint64_t g_timestamp         = 946656000000;  // 默认时间戳 2000-1-1 00:00:00



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 公有函数
/// ------------------------------------------------------------------------------------------------------------------------------------

/// @brief   打印 y_time 版本信息
void y_time_print_version() {
    YLOG_VERSION("y_time", Y_TIME_MAJOR, Y_TIME_MINOR, Y_TIME_PATCH);
}

/// @brief    获取 rtc 时间字符串
/// @return   时间字符串
char *y_time_get_str() {
    static char time_str[26] = {0};
    TIME_st     time         = y_time_get();
    sprintf(time_str, "[%04d.%02d.%02d %02d:%02d:%02d %03d]", time.year, time.mon, time.day, time.hour, time.min, time.sec, time.ms);
    return time_str;
}

/// @brief   获取 rtc 时间
/// @return  时间指针
TIME_st y_time_get() {

    // 获取 RTC 时间  并转换成时间戳
    TIME_st   time_1       = y_ll_time_get();
    struct tm tm_time_1    = {0};
    tm_time_1.tm_year      = time_1.year - 1900;
    tm_time_1.tm_mon       = time_1.mon - 1;
    tm_time_1.tm_mday      = time_1.day;
    tm_time_1.tm_hour      = time_1.hour;
    tm_time_1.tm_min       = time_1.min;
    tm_time_1.tm_sec       = time_1.sec;
    uint64_t tmp_timestamp = (mktime(&tm_time_1) - 28800) * 1000;  // 东8 区 减 28800 秒

    // 判断时间戳相差 大于 3s
    if (tmp_timestamp - g_timestamp > 3000 && tmp_timestamp - g_timestamp < -3000) {
        g_timestamp = tmp_timestamp;
    }

    // 时间戳 转化为 TIME_st 类型
    time_t     now_timestamp = (time_t) (g_timestamp / 1000) + 28800;  // 东8 区 加 28800 秒
    struct tm *tm_time_2     = localtime(&now_timestamp);
    TIME_st    time_2        = {0};
    time_2.year              = tm_time_2->tm_year + 1900;
    time_2.mon               = tm_time_2->tm_mon + 1;
    time_2.day               = tm_time_2->tm_mday;
    time_2.hour              = tm_time_2->tm_hour;
    time_2.min               = tm_time_2->tm_min;
    time_2.sec               = tm_time_2->tm_sec;
    time_2.ms                = g_timestamp % 1000;
    return time_2;
}

/// @brief   设置 rtc 时间
/// @param   [in] time                     要设置的时间
/// @retval  true                          成功
/// @retval  false                         失败
bool y_time_set(TIME_st *time) {
    return y_ll_time_set(time);
}

/// @brief   获取时间戳
/// @return  时间戳
uint64_t y_time_get_timestamp() {
    return g_timestamp;
}

/// @brief   设置时间戳
/// @param   [in] timestamp                时间戳
/// @retval  true                          成功
/// @retval  false                         失败
bool y_time_set_timestamp(uint64_t timestamp) {

    // 获取实际的时间戳
    timestamp                = timestamp < 9999999999 ? timestamp * 1000 : timestamp;              // 转化为 ms 时间戳
    timestamp                = timestamp < g_default_timestamp ? g_default_timestamp : timestamp;  // 设置最小时间戳为默认时间
    g_timestamp              = timestamp;

    // 时间戳 转化为 TIME_st 类型
    time_t     now_timestamp = (time_t) (g_timestamp / 1000) + 28800;  // 东8 区 加 28800 秒
    struct tm *now_time      = localtime(&now_timestamp);
    TIME_st    time          = {0};
    time.year                = now_time->tm_year + 1900;
    time.mon                 = now_time->tm_mon + 1;
    time.day                 = now_time->tm_mday;
    time.hour                = now_time->tm_hour;
    time.min                 = now_time->tm_min;
    time.sec                 = now_time->tm_sec;

    return y_time_set(&time);
}

/// @brief   时间滴答时钟
void y_time_tick() {
    g_timestamp++;
}
