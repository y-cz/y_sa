/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2024 ycz. All rights reserved.
///
/// Created by ycz on 2024/1/19.
///
/// @file  y_timer.h
///
/// @brief
///     timer 是对常用嵌入式平台定时器接口的封装库
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_TIMER_H
#define _Y_TIMER_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_TIMER_MAJOR 0  ///< 主版本     ( 主架构变化 )
#define Y_TIMER_MINOR 1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_TIMER_PATCH 0  ///< 补丁版本   ( bug 修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 枚举
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 定时器类型 枚举
typedef enum {
    TIMER_ONCE,      ///< 运行一次
    TIMER_PERIODIC,  ///< 周期运行
    TIMER_ALL,       ///< 计数
} TIMER_TYPE_e;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 定时器看门狗句柄
typedef struct {
    bool     is_running;    ///< 是否运行
    bool     status;        ///< 状态 是否喂过狗
    uint32_t time_ms;       ///< 看门狗时间 ms
    uint32_t countdown_ms;  ///< 看门狗计时 ms
} TIMER_DOG_st;

/// 定时器句柄
typedef struct {
    bool     is_running;    ///< 是否运行
    uint8_t  trigger_num;   ///< 触发次数
    uint8_t  type;          ///< 定时器类型
    uint32_t time_ms;       ///< 定时时间 ms
    uint32_t countdown_ms;  ///< 定时计时 ms
} TIMER_st;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void      y_timer_print_version();                                   ///< 打印模块版本号
void      y_timer_set_reboot_time(uint32_t time_ms, bool is_force);  ///< 设置定时重启
void      y_timer_dog_feed();                                        ///< 喂定时器看门狗
bool      y_timer_dog_create(uint32_t time_ms);                      ///< 创建一个定时器看门狗 主要用于低功耗产品
uint8_t   y_timer_get_trigger_num(TIMER_st *timer);                  ///< 获取定时器触发次数
bool      y_timer_stop(TIMER_st *timer);                             ///< 停止运行定时器
bool      y_timer_start(TIMER_st *timer, uint32_t time_ms);          ///< 开始运行定时器
bool      y_timer_restart(TIMER_st *timer, uint32_t time_ms);        ///< 重新运行定时器
bool      y_timer_destroy(TIMER_st *timer);                          ///< 销毁一个定时器
TIMER_st *y_timer_create(TIMER_TYPE_e type);                         ///< 创建一个定时器
void      y_timer_process(uint32_t time_ms);                         ///< 定时器任务处理



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_TIMER_H 防止当前头文件被重复引用