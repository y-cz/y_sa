/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2024 ycz. All rights reserved.
///
/// Created by ycz on 24-7-15.
///
/// @file  y_alg.h
///
/// @brief
///     一种应用于嵌入式的简单算法工具
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_ALG_H
#define _Y_ALG_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include <float.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_ALG_MAJOR 0  ///< 主版本     ( 主架构变化 )
#define Y_ALG_MINOR 1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_ALG_PATCH 0  ///< 补丁版本   ( bug 修复 )

#define Y_PI        3.14159265358979F  ///< PI


/// ------------------------------------------------------------------------------------------------------------------------------------
/// 枚举
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 坐标轴枚举
typedef enum {
    AXIS_NONE,  ///< 空
    AXIS_X,     ///< X轴
    AXIS_Y,     ///< Y轴
    AXIS_Z,     ///< Z轴
} ALG_AXIS_e;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

#pragma pack(1)  // 设置结构体 1 字节对齐

/// 卡尔曼参数结构体
typedef struct {
    double last_p;  ///< 上次估算协方差
    double now_p;   ///< 当前估算协方差
    double out;     ///< 卡尔曼滤波器输出
    double kg;      ///< 卡尔曼增益
    double q;       ///< 过程噪声协方差  q 增大，动态响应变快，收敛稳定性变坏
    double r;       ///< 观测噪声协方差  r 增大，动态响应变慢，收敛稳定性变好
} ALG_KALMAN_st;

/// IIR 参数结构体
typedef struct {
    double fs;      ///< 采样频率
    double fc;      ///< 截止频率
    double arr[2];  ///< 延迟缓存数组
    double a[2];    ///< 数组a
    double b[3];    ///< 数组b
} ALG_IIR_st;

#pragma anon_unions  // 解决匿名联合体报错问题

/// 窗口参数结构体
typedef struct {
    uint16_t size;       ///< 窗口大小
    uint16_t slide;      ///< 窗口滑动大小
    uint16_t index;      ///< 当前操作索引 从 1 开始
    bool     is_double;  ///< 是否是双精度数组 单精度使用 arr_f  双精度使用 arr_d
    union {
        float  *arr_f;  ///< 窗口数组 单精度
        double *arr_d;  ///< 窗口数组 双精度
    };
} ALG_WINDOW_st;

#pragma pack()  // 恢复结构体默认对其方式



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void           y_alg_print_version();                   ///< 打印模块版本号
double         y_alg_trunc(double value, uint8_t num);  ///< 截断小数
// 统计
uint16_t       y_alg_get_max_index(double *arr, uint16_t size);                           ///< 求最大值数值下标
uint16_t       y_alg_get_min_index(double *arr, uint16_t size);                           ///< 求最小值数值下标
double         y_alg_get_max(double *arr, uint16_t size);                                 ///< 求最大值
double         y_alg_get_min(double *arr, uint16_t size);                                 ///< 求最小值
double         y_alg_get_mid(double *arr, uint16_t size);                                 ///< 求中位值
double         y_alg_get_sum(double *arr, uint16_t size);                                 ///< 求和
double         y_alg_get_avg(double *arr, uint16_t size);                                 ///< 求平均值
double         y_alg_get_var(double *arr, uint16_t size);                                 ///< 求方差
double         y_alg_get_sd(double *arr, uint16_t size);                                  ///< 求标准差
double         y_alg_get_trimmean(double *arr, uint16_t size);                            ///< 去掉最大值最小值后求平均
double         y_alg_get_weighted_avg(double *arr, uint16_t size, double *weighted_arr);  ///< 求加权平均值
// 滤波
double         y_alg_filter_limiting(double now_value, double last_value, double threshold);           ///< 限幅滤波
double         y_alg_filter_first_order_low(double now_value, double last_value, double a);            ///< 一阶低通滤波
double         y_alg_filter_first_order_high(double now_value, double last_value, double a);           ///< 一阶高通滤波
double         y_alg_filter_first_order_complementary(double low_value, double high_value, double a);  ///< 一阶互补滤波
double         y_alg_filter_kalman(ALG_KALMAN_st *param, double now_value);                            ///< 卡尔曼滤波
double         y_alg_filter_iir(ALG_IIR_st *param, double now_value);                                  ///< IIR滤波 无限长冲激响应响应滤波器
// 窗口
ALG_WINDOW_st *y_alg_window_create(uint16_t size, uint16_t slide, bool is_double);  ///< 创建窗口
bool           y_alg_window_destroy(ALG_WINDOW_st *win);                            ///< 销毁窗口
bool           y_alg_window_is_empty(ALG_WINDOW_st *win);                           ///< 窗口数据是否为空
bool           y_alg_window_is_full(ALG_WINDOW_st *win);                            ///< 窗口数据是否为满
bool           y_alg_window_add(ALG_WINDOW_st *win, double value);                  ///< 往窗口添加值
bool           y_alg_window_del(ALG_WINDOW_st *win);                                ///< 从窗口删除值 从后往前删除
bool           y_alg_window_clear(ALG_WINDOW_st *win);                              ///< 清空窗口值
void           y_alg_window_print(ALG_WINDOW_st *win);                              ///< 打印窗口值



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_ALG_H 防止当前头文件被重复引用