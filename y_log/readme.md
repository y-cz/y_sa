<p align="left">
	<a target="_blank" href="https://gitee.com/y-cz/y_log/blob/master/LICENSE">
		<img src="https://img.shields.io/badge/license-MIT-green" ></img>
	</a>
</p>

## YLOG

### 1. YLOG 介绍

一种用于嵌入式设备简单调试的日志，可以方便的将需要输出的信息进行格式化显示。

### 2. YLOG 使用说明

YLOG 目前包含一个源码文件 y_log.h 和 y_log.c 文件，使用时仅需以下简单设置。

```c
// 在 y_log.h 中设置日志各功能开关
#define Y_LOG_SWITCH_TIME    0  ///< 日志时间打印开关                  0=关闭  1=打开
#define Y_LOG_SWITCH_COLOR   0  ///< 日志颜色打印开关                  0=关闭  1=打开
#define Y_LOG_SWITCH_ASSERT  1  ///< 断言日志打印开关 (Assert)         0=关闭  1=打开  2=只断言不打印
#define Y_LOG_SWITCH_ERROR   1  ///< 错误日志打印开关 (Error)          0=关闭  1=打开
#define Y_LOG_SWITCH_WARN    1  ///< 警告日志打印开关 (Warn)           0=关闭  1=打开
#define Y_LOG_SWITCH_INFO    1  ///< 信息日志打印开关 (Info)           0=关闭  1=打开
#define Y_LOG_SWITCH_DEBUG   1  ///< 调试日志打印开关 (Debug)          0=关闭  1=打开
#define Y_LOG_SWITCH_VERBOSE 1  ///< 详细日志打印开关 (Verbose)        0=关闭  1=打开
#define Y_LOG_SWITCH_DATA    1  ///< 数据日志打印开关 (Data)           0=关闭  1=打开
#define Y_LOG_SWITCH_VERSION 1  ///< 版本日志打印开关 (version)        0=关闭  1=打开
```

示例:

```c
#include "y_log.h"

int main() {

    // 开始测试
    YLOG_CRLF();
    YLOGI("------------------------------------------  y_log_test  start  ------------------------------------------");

    // 要带时间打印 请将 Y_LOG_SWITCH_TIME  设置为 1  并设置获取时间字符串函数 Y_TIME_STR
    // 要带颜色打印 请将 Y_LOG_SWITCH_COLOR 设置为 1

    // 1. 断言测试
    // 表达式为真  断言成功 程序继续执行
    // 表达式为假  断言失败 程序立即返回
    // YLOGA(1);        // 断言失败 则返回但无返回值
    YLOGA_FALSE(1);  // 断言失败 则返回 false
    // YLOGA_NULL(1);   // 断言失败 则返回 NULL

    // 2. 打印 各种格式日志 (结束后默认换行)
    YLOGE("HELLO,YLOG!!!");
    YLOGW("HELLO,YLOG!!!");
    YLOGI("HELLO,YLOG!!!");
    YLOGD("HELLO,YLOG!!!");
    YLOGV("HELLO,YLOG!!!");

    // 3.打印 16 进制格式数据 (结束后默认换行)
    unsigned char buf[8] = {1, 2, 3, 4, 5, 6, 7, 8};
    YLOG_DATA("DATA", buf, 8);

    // 4.打印模块版本号 (结束后默认换行)
    YLOG_VERSION("test_module", 1, 2, 3);

    // 5.打印初始化结果 (结束后默认换行)
    // 表达式为真  初始化成功
    // 表达式为假  初始化失败
    YLOG_INIT(true);

    // 6.打印换行
    YLOGI("LF");
    YLOG_LF();
    YLOGI("CR");
    YLOG_CR();
    YLOGI("CRLF");
    YLOG_CRLF();

    // 结束测试
    YLOGI("------------------------------------------  y_log_test  OK  ------------------------------------------");

    return 0;
}


// 输出结果:
[INFO ][0050][                        main] ----- < ------------------------------------------  y_log_test  start  ------------------------------------------ >
[ERROR][0063][                        main] ----- < HELLO,YLOG!!! >
[WARN ][0064][                        main] ----- < HELLO,YLOG!!! >
[INFO ][0065][                        main] ----- < HELLO,YLOG!!! >
[DEBUG][0066][                        main] ----- < HELLO,YLOG!!! >
[VER  ][0067][                        main] ----- < HELLO,YLOG!!! >
[DATA ][0071][                        main] ----- < DATA 8 byte :  [ 01 02 03 04 05 06 07 08 ]  >
[INFO ][0102][               y_log_version] ----- < V1.2.3  : module  test_module >
[INFO ][0079][                        main] ----- < init OK      true >
[INFO ][0082][                        main] ----- < LF >

[INFO ][0084][                        main] ----- < CR >
[INFO ][0086][                        main] ----- < CRLF >

[INFO ][0090][                        main] ----- < ------------------------------------------  y_log_test  OK  ------------------------------------------ >

```

### 3. 依赖

YLOG 无任何依赖

### 4. 版本说明

#### V0.2.1

1. 修复断言宏结尾分号不统一的问题

#### V0.2.0

1. 添加 y_log.c 文件，将宏实现放到函数中，减少代码体积

#### V0.1.11

1. 修改日志开关，由等级控制改为单独控制
2. 更新文档描述

#### V0.1.10

1. 增加断言宏 YLOGA_INIT 判断初始化是否成功，成功或者失败都打印一条日志
2. 增加宏 YLOG_VERSION 打印版本信息

#### V0.1.9

1. 增加断言宏 YLOGA_NULL、YLOGA_TRUE、YLOGA_FALSE，断言条件满足后直接返回

#### V0.1.8

1. 增加断言宏 YLOGA，断言某个表达式是否为真
2. 增加时间打印，使用时需要配置获取时间戳 API
3. 修改颜色打印宏，减少宏判断，优化代码逻辑

#### V0.1.7

1. 修改 YLOG_DATA 宏打印格式,改为一行打印

#### V0.1.6

1. 修改默认打印为 Y_LEVEL_DEBUG 等级, 解决初次使用打印内容过多的问题

#### V0.1.5

1. 增加回车换行打印宏，YLOG_LF(), YLOG_CR(), YLOG_CRLF()
2. readme.md 增加使用示例

#### V0.1.4

1. 修改数据打印时循环变量命名，避免与常用循环变量 i 发生冲突

#### V0.1.3

1. 取消使能宏，更改通过为设置日志等级 Y_LEVEL_NONE 关闭日志打印，解决某些情况下突然关闭使能导致编译不过
2. 增加数据日志打印功能，将需处理的数据按字节打印出来

#### V0.1.2

1. 文件中添加 MIT 许可证
2. 修改注释风格和部分宏名称，使阅读时更容易理解
3. USE_Y_LOG 和 Y_LOG_USE_COLOR 从定义使能更改为 0/1 值使能
4. 代码行数显示由固定 3 位改为固定 4 位,解决文件行数大于 1000 时打印对不齐的问题
5. 打印内容前后各增加一个空格
6. 版本号打印由函数变为内联函数，并删除 y_log.c 文件，方便使用

#### V0.1.1

1. 修改宏定义名称，防止与其他日志系统产生冲突。
2. 修改一些对齐格式，使格式更简单美观，方便阅读。

#### V0.1.0

1. 格式化输出 日志等级，行号，函数名，日志信息 等内容。
2. 可静态配置输出日志等级。
3. 可以选择是否带颜色输出，并可单独设置不同日志的输出颜色。