/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2024 ycz. All rights reserved.
///
/// Created by ycz on 24-2-3.
///
/// @file  y_log.c
///
/// @brief
///     y_log 是一种用于嵌入式设备简单调试的日志，可以方便的将需要输出的信息进行格式化显示。
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include "y_log.h"

#include <stdarg.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 公有函数
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 打印 y_log 版本信息
void y_log_print_version() {
    YLOG_VERSION("Y_LOG", Y_LOG_VERSION_MAJOR, Y_LOG_VERSION_MINOR, Y_LOG_VERSION_PATCH);
}

/// 打印常规日志
void y_log_print(uint8_t log_type, int line, const char *fun, char *buf, ...) {

    // 解析可变参数
    char    log_str[512] = {0};  // 一般为一行的最大长度
    va_list v;
    va_start(v, buf);
    vsprintf(log_str, buf, v);  // 使用可变参数的字符串打印。类似sprintf
    va_end(v);

    // 判断打印日志类型
    if (log_type == 1) {
#if (Y_LOG_SWITCH_ASSERT == 1)
        printf(Y_COLOR_ASSERT "%s[ASSE ][%04d][%28.28s] ----- < %s >" Y_COLOR_END "\r\n", Y_TIME_STR, line, fun, log_str);
#endif
    } else if (log_type == 2) {
#if Y_LOG_SWITCH_ERROR
        printf(Y_COLOR_ERROR "%s[ERROR][%04d][%28.28s] ----- < %s >" Y_COLOR_END "\r\n", Y_TIME_STR, line, fun, log_str);
#endif
    } else if (log_type == 3) {
#if Y_LOG_SWITCH_WARN
        printf(Y_COLOR_WARN "%s[WARN ][%04d][%28.28s] ----- < %s >" Y_COLOR_END "\r\n", Y_TIME_STR, line, fun, log_str);
#endif
    } else if (log_type == 4) {
#if Y_LOG_SWITCH_INFO
        printf(Y_COLOR_INFO "%s[INFO ][%04d][%28.28s] ----- < %s >" Y_COLOR_END "\r\n", Y_TIME_STR, line, fun, log_str);
#endif
    } else if (log_type == 5) {
#if Y_LOG_SWITCH_DEBUG
        printf(Y_COLOR_DEBUG "%s[DEBUG][%04d][%28.28s] ----- < %s >" Y_COLOR_END "\r\n", Y_TIME_STR, line, fun, log_str);
#endif
    } else if (log_type == 6) {
#if Y_LOG_SWITCH_VERBOSE
        printf(Y_COLOR_VERBOSE "%s[VER  ][%04d][%28.28s] ----- < %s >" Y_COLOR_END "\r\n", Y_TIME_STR, line, fun, log_str);
#endif
    }
}

/// 打印数据日志
void y_log_data(int line, const char *fun, char *name, uint8_t *data, uint16_t size) {
    printf(Y_COLOR_DATA "%s[DATA ][%04d][%28.28s] ----- < %s %d byte :  [ ", Y_TIME_STR, line, fun, name, size);
    for (int i = 0; i < (size); ++i) {
        printf("%02x ", (data)[i]);
    }
    printf("]  >" Y_COLOR_END "\r\n");
}

/// 打印模块版本信息
void y_log_version(char *MODULE, uint8_t MAJOR, uint8_t MINOR, uint8_t PATCH) {
    YLOGI("V%d.%d.%-2d : module  %s", MAJOR, MINOR, PATCH, MODULE);
}
