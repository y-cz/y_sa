/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2023 ycz. All rights reserved.
///
/// Created by ycz on 2023/10/26.
///
/// @file  y_sys.h
///
/// @brief
///     y_sys 是一些系统属性和功能的抽象
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_SYS_H
#define _Y_SYS_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include "y_utils.h"



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_SYS_MAJOR 0  ///< 主版本     ( 主架构变化 )
#define Y_SYS_MINOR 1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_SYS_PATCH 0  ///< 补丁版本   ( bug 修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 系统信息结构体
typedef struct {
    VERSION_st sw_ver;    ///< 软件版本
    VERSION_st hw_ver;    ///< 硬件版本
    uint8_t    dev_type;  ///< 设备类型
    uint16_t   dev_num;   ///< 设备编号
    uint8_t    mac[6];    ///< mac 地址
    uint8_t    sn[10];    ///< sn 序列号
} SYS_CONFIG_st;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void          y_sys_print_version();                 ///< 打印模块版本号
void          y_sys_print_software_version();        ///< 打印 软件 版本号
void          y_sys_print_hardware_version();        ///< 打印 硬件 版本号
void          y_sys_print_config();                  ///< 打印系统配置信息
SYS_CONFIG_st y_sys_get_config();                    ///< 获取系统配置信息
VERSION_st    y_sys_get_software_version();          ///< 获取 软件 版本号
VERSION_st    y_sys_get_hardware_version();          ///< 获取 硬件 版本号
uint8_t       y_sys_get_dev_type();                  ///< 获取 设备类型
uint16_t      y_sys_get_dev_num();                   ///< 获取 设备编号
uint8_t      *y_sys_get_mac();                       ///< 获取 MAC 地址
uint8_t      *y_sys_get_mac_str();                   ///< 获取 MAC 地址字符串
uint8_t      *y_sys_get_sn();                        ///< 获取 sn
uint8_t      *y_sys_get_sn_str();                    ///< 获取 sn 字符串
bool          y_sys_set_dev_type(uint8_t dev_type);  ///< 设置 设备类型
bool          y_sys_set_dev_num(uint16_t num);       ///< 设置 设备编号
bool          y_sys_set_sn(uint8_t *sn);             ///< 设置 sn
bool          y_sys_init();                          ///< 初始化



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_SYS_H 防止当前头文件被重复引用