/// ------------------------------------------------------------------------------------------------------------------------------------
///
/// MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in all
/// copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
/// SOFTWARE.
///
/// Copyright (c) 2023 ycz. All rights reserved.
///
/// Created by ycz on  2022/1/2.
///
/// @file  y_cli.h
///
/// @brief
///     y_cli 是一种简单的串口命令行处理
///
/// ------------------------------------------------------------------------------------------------------------------------------------



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 防止当前头文件被重复引用
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifndef _Y_CLI_H
#define _Y_CLI_H



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 头文件
/// ------------------------------------------------------------------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 兼容 c++
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 宏定义
/// ------------------------------------------------------------------------------------------------------------------------------------

#define Y_CLI_MAJOR 0  ///< 主版本     ( 主架构变化 )
#define Y_CLI_MINOR 1  ///< 次版本     ( 单个功能增加或修改 )
#define Y_CLI_PATCH 0  ///< 补丁版本   ( bug 修复 )



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 结构体
/// ------------------------------------------------------------------------------------------------------------------------------------

/// 命令行配置结构体
typedef struct {
    bool     is_open;                              ///< 默认是否打开
    bool     is_fflush;                            ///< 是否调用 fflush 函数刷新
    uint16_t open_time_s;                          ///< 打开状态时长 S
    void (*cli_cb)(uint8_t *data, uint16_t size);  ///< 回调函数
} CLI_CONFIG_st;



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 函数 API
/// ------------------------------------------------------------------------------------------------------------------------------------

void           y_cli_print_version();                      ///< 打印模块版本号
bool           y_cli_get_status();                         ///< 获取状态 是否在进行命令行控制
bool           y_cli_delete_data(uint16_t size);           ///< 删除待解析数据
bool           y_cli_parse(uint8_t *data, uint16_t size);  ///< 解析数据
void           y_cli_config_print(CLI_CONFIG_st *config);  ///< 打印参数
CLI_CONFIG_st *y_cli_config_get();                         ///< 获取参数
bool           y_cli_config_set(CLI_CONFIG_st *config);    ///< 设置参数
bool           y_cli_init();                               ///< 初始化
void           y_cli_process(uint32_t time_ms);            ///< 任务处理



/// ------------------------------------------------------------------------------------------------------------------------------------
/// 条件编译结尾
/// ------------------------------------------------------------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif  // __cplusplus 兼容 c++
#endif  // _Y_CLI_H 防止当前头文件被重复引用